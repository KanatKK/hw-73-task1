const express = require("express");
const app = express();

app.get("/:name" , (req, res) => {
    res.send(req.params.name);
});

app.listen(8000, () => {});